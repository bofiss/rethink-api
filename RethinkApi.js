'use strict';

let thinky = require('thinky')({
    host: 'localhost',
    port: 28015,
    db: 'People'
});

let r = thinky.r;

let People = thinky.createModel('People', {
    firstName: String,
    lastName: String,
    coolnessFactor: Number,
    date: {_type: Date, default: r.now()}
});

People.ensureIndex('date');

exports.list = (req, res ) => {
    People.orderBy({index: r.desc('date')})
        .run()
        .then((people) => {
            res.json(people)
        }).error((err)=> {
            res.json({message: err});
        });

}


exports.add = (req, res) => {
    let person = new People(req.body);
    person.save()
          .then((result) => {
            res.json(result)
          }).error((err) => {
            res.json({messagege: err});
          });

};

exports.get = (req, res) => {
    People.get(req.params.id)
          .run()
          .then((person) => {
            res.json(person)
          }).error((err) => {
            res.json({message: err});
          });
};

exports.delete =  (req, res) =>{
    People.get(req.params.id)
          .run()
          .then((person) => {
            person.delete()
                  .then((result) => {
                    res.json(result)
                  }).error((err) => {
                    res.json({message: err});
              });
          }).error((err) => {
            res.json({message: err});
          });
};

exports.update =  (req, res) => {
   People.get(req.params.id)
          .run()
          .then((person) => {
            if(req.body.firstName) {
                person.firstName = req.body.firstName;
            }
            if(req.body.lastName) {
                person.lastName = req.body.lastName;
            }
            if(req.body.coolnessFactor) {
                person.coolnessFactor = parseInt(req.body.coolnessFactor);
            }
            person.date = r.now();
            person.save()
                  .then((result) => {
                    res.json(result)
                  }).error((err) => {
                    res.json({message: err});
                  });
          }).error((err) => {
            res.json({message: err});
          });

};
